import { productService } from "../service/productService";
import { Request, Response } from 'express';
import { log } from "../common/aop";
import IProductResponse from "../dto/productResponse";

export class ProductController {

    @log
    public async getProduct(req: Request, res: Response) {
        const id = req.params.productId;
        if (id.match(/^[0-9a-fA-F]{24}$/)) {
            let product = await productService.getProduct(id);
            const productRes: IProductResponse = {
                message: 'GET request successfulll!!!!',
                value: product
            }
            res.status(200).json(productRes);
        } else {
            res.status(200).json({
                message: 'id is a valid ObjectId',
            });
        }
    }

    @log
    public async getAllProduct(req: Request, res: Response) {
        let productAll = await productService.getAllProduct();
        const productRes: IProductResponse = {
            message: 'GET request successfulll!!!!',
            value: productAll
        }
        res.status(200).json(productRes);
    }
}

export const productController = new ProductController();