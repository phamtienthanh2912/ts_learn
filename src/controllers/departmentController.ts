import { Request, Response } from 'express';
import { getDb } from '../utils/dbConnection'
import * as OracleDB from 'oracledb';
import { IDepartment } from '../dto/department';

export class DepartmentController {

    public async getDepartment(req: Request, res: Response) {
        let conn = await getDb();
        try {
            const result = await conn.execute(`SELECT * FROM emp`, {}, { outFormat: OracleDB.OUT_FORMAT_OBJECT });
            res.status(200).json(result.rows);
        } catch (err) {
            console.error(err);
            res.status(500).json(err);
        } finally {
            if (conn) {
                try {
                    await conn.close();
                } catch (err) {
                    console.error(err);
                }
            }
        }
    }

    public async addDepartment(req: Request, res: Response) {
        // const department = Object.assign({}, req.body)
        // console.log(department)
        let dept : IDepartment =  req.body;
        console.log(dept)
        let dataInsert = {
            "id" : dept.id,
            "name" : dept.name,
            "code" : dept.code
        }
        let conn = await getDb();
        try {
            const result = await conn.execute(`INSERT INTO department ( id, name, code) VALUES (:id, :name, :code)`, 
            dataInsert, {autoCommit: false});
            await conn.commit();
            res.status(200).json(result);
        } catch (err) {
            console.error(err);
            res.status(500).json(err);
        } finally {
            if (conn) {
                try {
                    await conn.close();
                } catch (err) {
                    console.error(err);
                }
            }
        }
    }
}

export const departmentController = new DepartmentController();