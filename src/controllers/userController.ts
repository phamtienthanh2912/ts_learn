import { userService } from "../service/userService";
import { Request, Response } from 'express';
import { log } from "../common/aop";
import IUserResponse from "../dto/userResponse";

export class UserController {

    @log
    public async getUserByEmail(req: Request, res: Response) {
        const email = req.body.email;
        let user = await userService.getUserByEmail(email);
        const userRes: IUserResponse = {
            message: 'GET request successfulll!!!!',
            value: user
        }
        res.status(200).json(userRes);
    }
}

export const userController = new UserController();