import { Request, Response } from 'express';
import { log } from "../common/aop";

class HomeController {

    @log
    public async home(req: Request, res: Response) {
        res.status(200).json({
            message: 'GET request successfulll!!!!',
        })
    }
}

export const homeController = new HomeController();