import { Product } from "../models/product";
import { productRepository } from "../repository/productRepository";
import * as _ from "lodash";

export class ProductService {

    async getProduct(id: string): Promise<Product | null> {
        return productRepository.getOne(id);
    }

    async getAllProduct(): Promise<Product[] | null> {
        console.log(_.padStart("Hello TypeScript!", 20, " "));
        const lstProduct = await productRepository.getAll();
        // let lstProductCustom = lstProduct.map(item => {
        //     return {
        //         name: "Pham Thanh"
        //     }
        // });
        lstProduct.forEach(item => {
            item.name = "Pham Thanh";
        });
        return lstProduct;
    }
    
}

export const productService = new ProductService();

