import { userRepository } from "../repository/userRepository";
import { User } from "../models/user";

export class UserService {

    public async getUserByEmail(email: string): Promise<User | null> {
        return await userRepository.getUserByEmail(email);
    }
}

export const userService = new UserService();