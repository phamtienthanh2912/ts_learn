import { User } from "models/user";

export default interface IUserResponse {
    message: string;
    value: User | User[] | null;
}