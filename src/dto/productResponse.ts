import { Product } from "../models/product";

export default interface IProductResponse {
    message: string;
    value: Product | Product[] | null;
}