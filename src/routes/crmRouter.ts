import { Application } from "express";
import { productController } from "../controllers/productController";
import { homeController } from "../controllers/homeController";
import { userController } from "../controllers/userController";
import { departmentController } from "../controllers/departmentController";

export class Routes {
    public routes(app: Application): void {
        app.route('/').get(homeController.home);
        app.route('/all/:productId').get(productController.getProduct);
        app.route('/product/all').get(productController.getAllProduct);
        app.route('/user').post(userController.getUserByEmail);
        app.route('/department').get(departmentController.getDepartment);
        app.route('/department').post(departmentController.addDepartment);
    }
}