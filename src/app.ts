import * as express from "express";
import * as bodyParser from "body-parser";
import { Routes } from "./routes/crmRouter";
//import { db } from "./common/db";
import { connectDb } from './utils/dbConnection'

class App {

    public app: express.Application;
    public routePrv: Routes = new Routes();

    constructor() {
        this.app = express();
        this.config();       
        this.routePrv.routes(this.app);
        //db.connect();
        connectDb();
    }

    private config(): void {
        // support application/json type post data
        this.app.use(bodyParser.json());
        //support application/x-www-form-urlencoded post data
        this.app.use(bodyParser.urlencoded({ extended: false }));
    }

}

export default new App().app;