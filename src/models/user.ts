import * as mongoose from "mongoose";
import { prop, Typegoose } from "typegoose";

export class User extends Typegoose {
  public _id?: mongoose.Types.ObjectId;

  @prop({ required: true })
  public name: string;

  @prop({ required: true })
  public email: string;

  @prop({ required: true })
  public password: string;

  @prop({ required: true })
  public date: Date;

}
export const userModel = new User().getModelForClass(User, {
  existingMongoose: mongoose,
  schemaOptions: { collection: "users" }
});