import * as mongoose from "mongoose";
import { prop, Typegoose } from "typegoose";

export class Product extends Typegoose {
  _id?: mongoose.Types.ObjectId;

  @prop({ required: true })
  name: string;

  @prop({ required: true })
  displayName: string;

  @prop({ required: true })
  externalProductId: string;

  @prop({ required: true })
  vendor: string;

  description: string;

}
export const productModel = new Product().getModelForClass(Product, {
  existingMongoose: mongoose,
  schemaOptions: { collection: "products" }
});