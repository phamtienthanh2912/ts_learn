
import { Product, productModel } from "../models/product";

export class ProductRepository {
    async getOne(id: string): Promise<Product | null> {
        return productModel.findById(id);
    }

    async create(doc: Product): Promise<Product> {
        return productModel.create(doc);
    }

    async getAll(): Promise<Product[]> {
        return productModel.find();
    }
}

export const productRepository = new ProductRepository();