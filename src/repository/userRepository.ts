import IRepository from "../common/IRepository";
import { User, userModel } from "../models/user";

export class UserRepository implements IRepository<User> {

    public async getOne(id: string): Promise<User | null> {
        return userModel.findById(id);
    }

    public async create(user: User): Promise<User> {
        return new User();
    }

    public async getAll(): Promise<User[] | null> {
        return null;
    }

    public async getUserByEmail(email: string): Promise<User | null> {
        return userModel.findOne({'email': email, "status": "1"});
    }

}

export const userRepository = new UserRepository();

