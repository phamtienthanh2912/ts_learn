import * as OracleDB from 'oracledb';

let connection : OracleDB.Connection;
let pool : OracleDB.Pool;
const mypw = 'scott';
export async function connectDb() {
    console.log('create oracle pool');
    pool = await OracleDB.createPool({
        user: "SCOTT",
        password: mypw,
        connectString: "27.118.22.14:1521/orcl",
        poolMax : 50,
        poolMin : 5,
        poolAlias : 'Test-PoolOracle',
        poolIncrement : 1,
        poolTimeout: 60
    });
    console.log(pool);
}

export async function getDb() : Promise<OracleDB.Connection> {
    connection = await pool.getConnection();
    return connection;
}