import * as mongoose from "mongoose";
import { logger } from "./logger";

export function log(target: any, key: string, descriptor: PropertyDescriptor) {
  const originalMethod = descriptor.value;
  descriptor.value = async function() {
    logger.debug(`${key} was called with: `, arguments);
    try {
      const result = await originalMethod.apply(this, arguments);
      logger.debug("result:  ", result);
      return result;
    } catch (e) {
      logger.error("Unexpected error occurred: ", e);
      if (e instanceof mongoose.Error.ValidationError) {
        logger.error(`Mongoose validation error ${JSON.stringify(e)}`);
        return null;
      }
      logger.error(`Internal server error ${JSON.stringify(e)}`);
      return null;
    }
  };
  return descriptor;
}

