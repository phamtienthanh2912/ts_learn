export default interface IRepository<T> {
    getOne(id: string): Promise<T | null>;
    create(entity: T): Promise<T>;  
    getAll() : Promise<T[] | null>;
  }