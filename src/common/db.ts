import { MongoError } from "mongodb";
import * as mongoose from "mongoose";
import { logger } from "./logger";

export enum ConnectionStatus {
  NOT_CONNECTED,
  CONNECTED,
  FAILED
}

export class Db {
  public status: ConnectionStatus = ConnectionStatus.NOT_CONNECTED;
  private mongoDb: Promise<mongoose.Mongoose>;

  public url: string = "mongodb://abc123!:abc123!@ds149974.mlab.com:49974/customer_api"; 

  public async connect(): Promise<mongoose.Mongoose> {
    logger.info("Connecting to Mongo DB ...");
    logger.info("Mongo URI from configuration: " + this.url);
    //logger.info("Mongo SSL from configuration: " + process.env.mongoSsl);
    if (this.status !== ConnectionStatus.CONNECTED) {
      this.mongoDb = mongoose.connect(
        this.url,
        (err: MongoError) => {
          if (err !== undefined && err !== null) {
            logger.error("Mongo connecton error: " + err);
            this.status = ConnectionStatus.FAILED;
          } else {
            logger.info("Mongo connection succeeded");
            this.status = ConnectionStatus.CONNECTED;
          }
        }
      );
    }
    return this.mongoDb;
  }
}

export const db = new Db();
