import * as winston from "winston";

export const logger = winston.createLogger({
  defaultMeta: { service: "product-service" },
  format: winston.format.combine(
    winston.format.timestamp(),
    winston.format.simple()
  ),
  level: process.env.logLevel ? process.env.logLevel : "info",
  transports: [new winston.transports.Console()]
});
