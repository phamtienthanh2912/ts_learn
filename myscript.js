const oracledb = require('oracledb');

oracledb.outFormat = oracledb.OUT_FORMAT_OBJECT;
const mypw = 'scott';  // set mypw to the hr schema password

async function run() {

  let connection;

  try {
    oracledb.autoCommit = true;
    //oracledb.outFormat = oracledb.OUT_FORMAT_OBJECT;
    connection = await oracledb.getConnection(  {
      user          : "SCOTT",
      password      : mypw,
      connectString : "27.118.22.14:1521/orcl"
    });
    let department = {
        id : 1,
        name : 'A',
        code : 'A'
    }
    // await connection.execute(`INSERT INTO department ( id, name, code) VALUES (:id, :name, :code)`,
    //     department);
    // await connection.commit();
    const result = await connection.execute(
      `SELECT name, code, id FROM department`
    );
    console.log(result.rows);


    // const soda = connection.getSodaDatabase();
    // const collectionName = 'department';
    // const myCollection = await soda.createCollection(collectionName);
    // const myDocuments  = await myCollection.find().getDocuments();
    // myDocuments.forEach(function(element) {
    //   const content = element.getContent();
    //   console.log(content.name + ' lives in Melbourne.');
    // });

  } catch (err) {
    console.error(err);
    rollbach();
  } finally {
    if (connection) {
      try {
        await connection.close();
      } catch (err) {
        console.error(err);
      }
    }
  }
}

const rollbach = async (connection) => {
  if (connection) {
    try {
      await connection.rollbach();
    } catch (err) {
      console.error(err);
    }
  }
}

run();